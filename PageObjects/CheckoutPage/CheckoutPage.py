# coding=utf-8
from CheckoutPage_selectors import CheckoutPageSelectors


class CheckoutPage(object):
    """
    Actions of the checkout page.
    """
    def __init__(self, driver):
        self.driver = driver
        self.locator = CheckoutPageSelectors

    """ INPUT ACTIONS """
    def enter_firstname(self, firstname):
        """
        First clears the firstname field and then enters the given text.
        :param firstname: firstname value to be entered.
        :return: None.
        """
        self.driver.find_element(*self.locator.first_name_field).clear()
        self.driver.find_element(*self.locator.first_name_field).send_keys(firstname)

    def enter_lastname(self, lastname):
        """
        First clears the lastname field and then enters the given text.
        :param lastname: lastname value to be entered.
        :return: None.
        """
        self.driver.find_element(*self.locator.last_name_field).clear()
        self.driver.find_element(*self.locator.last_name_field).send_keys(lastname)

    def enter_postal_code(self, postal_code):
        """
        First clears the postal code field and then enters the given text.
        :param postal_code: postal code value to be entered.
        :return: None.
        """
        self.driver.find_element(*self.locator.postal_code_field).clear()
        self.driver.find_element(*self.locator.postal_code_field).send_keys(postal_code)

    """ CLICKS """
    def click_continue_button(self):
        """
        Clicks the continue button.
        :return: None.
        """
        self.driver.find_element(*self.locator.continue_button).click()

    def click_checkout_button(self):
        """
        Clicks the checkout button.
        :return: None.
        """
        self.driver.find_element(*self.locator.checkout_button).click()

    """ GETTERS """
    def get_checkout_message(self):
        """
        Returns the checkout message.
        :return: Checkout message.
        """
        return self.driver.find_element(*self.locator.checkout_complete_label).text

    """ CHECKS """
    def is_checkout_successful(self):
        """
        Checks whether the checkout is successful or not.
        :return: true -> If the checkout result label contains the word 'THANK YOU'.
                 false -> If the checkout result label does not contain the word 'THANK YOU'.
        """
        return "THANK YOU" in self.get_checkout_message()

    def is_error_message_present(self):
        """
        Checks whether an error message is present on the checkout page or not.
        :return: true -> If the error message is present on the checkout page.
                 false -> If the error message is not present on the checkout page.
        """
        return self.driver.find_element(*self.locator.checkout_error_message).is_displayed()