# coding=utf-8
from selenium.webdriver.common.by import By


class CheckoutPageSelectors(object):
    """
    Selectors of the checkout page.
    """

    """ LABELS - TEXT """
    checkout_complete_label = (By.CLASS_NAME, "complete-header")
    checkout_error_message = (By.CLASS_NAME, "error-button")

    """ INPUT FIELDS """
    first_name_field = (By.XPATH, "//input[@data-test='firstName']")
    last_name_field = (By.XPATH, "//input[@data-test='lastName']")
    postal_code_field = (By.XPATH, "//input[@data-test='postalCode']")

    """ BUTTONS """
    checkout_button = (By.CLASS_NAME, "cart_checkout_link")
    remove_from_cart_button = (By.CLASS_NAME, "remove-from-cart-button")
    continue_button = (By.CLASS_NAME, "cart_checkout_link")

