# coding=utf-8
from LoginPageSelectors import LoginPageSelectors


class LoginPage(object):
    """
    Actions of the login page.
    """
    def __init__(self, driver):
        self.driver = driver
        self.locator = LoginPageSelectors
        self.url = "https://www.saucedemo.com/index.html"

    """ INPUT ACTIONS """
    def enter_username(self, username):
        """
        First clears the username field and then enters the given text.
        :param username: username value to be entered.
        :return: None.
        """
        self.driver.find_element(*self.locator.username_field).clear()
        self.driver.find_element(*self.locator.username_field).send_keys(username)

    def enter_password(self, password):
        """
        First clears the password field and then enters the given text.
        :param password: password value to be entered.
        :return: None.
        """
        self.driver.find_element(*self.locator.password_field).clear()
        self.driver.find_element(*self.locator.password_field).send_keys(password)

    """ GETTERS """
    def get_error_message(self):
        """
        Returns the error message.
        :return: Error message.
        """
        return self.driver.find_element(*self.locator.error_message).text

    """ CLICKS """
    def click_login_button(self):
        """
        Clicks the login button.
        :return: None.
        """
        self.driver.find_element(*self.locator.login_button).click()

    """ GENERIC ACTIONS """
    def login(self, username, password):
        """
        Generic login function. Enters the given username and password to the respective fields, clicks login button.
        :param username: Username to be entered.
        :param password: Password to be entered.
        :return: None.
        """
        self.enter_username(username)
        self.enter_password(password)
        self.click_login_button()

    def get_url(self):
        """
        Navigates to the login page.
        :return: None.
        """
        self.driver.get(self.url)

    """ CHECKS """
    def is_error_message_present(self):
        """
        Checks whether the invalid login error message is displayed on the page or not.
        :return: true -> If the error message is displayed.
                 false -> If the error message is not displayed.
        """
        return self.driver.find_element(*self.locator.error_message).is_displayed()


