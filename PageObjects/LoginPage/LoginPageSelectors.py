# coding=utf-8
from selenium.webdriver.common.by import By


class LoginPageSelectors(object):
    """
    Selectors of the login page.
    """

    """ INPUT FIELDS """
    username_field = (By.CSS_SELECTOR, "input[data-test=username]")
    password_field = (By.CSS_SELECTOR, "input[data-test=password]")

    """ BUTTONS """
    login_button = (By.CLASS_NAME, "login-button")

    """ LABELS - TEXT """
    error_message = (By.CSS_SELECTOR, "h3[data-test=error]")
