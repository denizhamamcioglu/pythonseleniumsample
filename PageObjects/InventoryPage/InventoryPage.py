# coding=utf-8
from InventoryPage_selectors import InventoryPageSelectors


class InventoryPage(object):
    """
    Actions of the inventory page
    """
    def __init__(self, driver):
        self.driver = driver
        self.locator = InventoryPageSelectors

    """ CLICK ACTIONS """
    def click_add_to_cart_button(self, order):
        """
        Clicks the add to cart button of nth item.
        :param order: Index of the inventory item that will be added to the shopping cart.
        :return: None.
        """
        self.driver.find_elements(*self.locator.add_to_cart_button)[order].click()

    def click_remove_from_cart_button(self, order):
        """
        Clicks the remove button of the nth item.
        :param order: Index of the inventory item that will be removed from the cart.
        :return: None.
        """
        self.driver.find_elements(*self.locator.remove_from_cart_button)[order].click()

    def click_reset_app_state_button(self):
        """
        Clicks the reset app state button within the menu.
        :return: None.
        """
        self.driver.find_element(*self.locator.reset_app_state).click()

    def click_menu_button(self):
        """
        Clicks the menu button.
        :return: None.
        """
        self.driver.find_element(*self.locator.menu_button).click()

    def click_item_name(self, order):
        """
        Clicks the item name area of the nth item.
        :param order: Index of the inventory item that will be clicked.
        :return: None.
        """
        self.driver.find_elements(*self.locator.item_name)[order].click()

    def click_shopping_cart(self):
        """
        Clicks the shopping cart item on the top right corner of the page.
        :return: None.
        """
        self.driver.find_element(*self.locator.shopping_cart).click()

    """ GETTERS """
    def get_item_price(self, order):
        """
        Returns the item price of the nth item.
        :return: Item price of the nth item.
        """
        return self.driver.find_elements(*self.locator.item_price)[order].text

    def get_shopping_cart_count(self):
        """
        Returns the shopping cart count from the top right corner of the page.
        :return: Shopping cart count.
        """
        return self.driver.find_element(*self.locator.shopping_cart).text

    def get_item_description(self, order):
        """
        Returns the item description of the nth item.
        :param order: Index of the item description that will be returned.
        :return: Item description.
        """
        return self.driver.find_elements(*self.locator.item_description)[order].text

    def get_item_name(self, order):
        """
        Returns the item name of the nth item.
        :param order: Index of the item name that will be returned.
        :return: Item name.
        """
        return self.driver.find_elements(*self.locator.item_name)[order].text

    def get_item_name_details(self):
        """
        Returns the item title in the item details page.
        :return: Item title in item details page.
        """
        return self.driver.find_element(*self.locator.item_name_detail).text

    def get_item_description_details(self):
        """
        Returns the item description on the item details page.
        :return: Item description in item details page.
        """
        return self.driver.find_element(*self.locator.item_description_detail).text

    def get_item_price_details(self):
        """
        Returns the item price in the item details page.
        :return: Item price in item details page.
        """
        return self.driver.find_element(*self.locator.item_price_detail).text

    """ CHECKS """
    def is_item_added_to_cart(self):
        """
        Checks whether the item can be added to the shopping cart or not.
        :return: True -> If the item is added to the shopping cart, meaning shopping cart count is greater than 0.
                 False -> Shopping cart count not available or equals to 0 or less than 0 (not possible).
                 Item could not be added to the shopping cart.
        """
        return self.get_shopping_cart_count() > "0"

    def is_item_removed_from_cart(self):
        """
        Checks whether the item can be removed from the shopping cart or not.
        :return: True -> If the item is removed from the shopping cart, meaning shopping cart count is not available.
                 False -> If the item cannot be removed from the shopping cart, meaning the shopping cart count is
                 still available.
        """
        return len(self.driver.find_element(*self.locator.shopping_cart).text) == 0
