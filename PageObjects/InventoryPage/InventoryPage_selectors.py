# coding=utf-8
from selenium.webdriver.common.by import By


class InventoryPageSelectors(object):
    """
    Selectors of the inventory page.
    """

    """ LABELS - TEXT """
    inventory_item = (By.CLASS_NAME, "inventory_item")
    item_price = (By.CLASS_NAME, "inventory_item_price")
    item_name = (By.CLASS_NAME, "inventory_item_name")
    item_description = (By.CLASS_NAME, "inventory_item_desc")
    shopping_cart = (By.ID, "shopping_cart_container")
    item_name = (By.CLASS_NAME, "inventory_item_name")
    item_name_detail = (By.CLASS_NAME, "inventory_details_name")
    item_description_detail = (By.CLASS_NAME, "inventory_details_desc")
    item_price_detail = (By.CLASS_NAME, "inventory_details_price")

    """ BUTTONS """
    add_to_cart_button = (By.CLASS_NAME, "add-to-cart-button")
    remove_from_cart_button = (By.CLASS_NAME, "remove-from-cart-button")
    menu_button = (By.CLASS_NAME, "bm-burger-button")
    logout_button = (By.ID, "logout_sidebar_link")
    reset_app_state = (By.ID, "reset_sidebar_link")
