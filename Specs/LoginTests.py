# coding=utf-8
import unittest
from selenium import webdriver
from PageObjects.LoginPage import LoginPage
import UserCredentials


class LoginTests(unittest.TestCase):
    def setUp(self):
        """
        Login related test cases.
        setUp block will be executed before each test case.
        """
        self.driver = webdriver.Chrome()
        self.loginPage = LoginPage.LoginPage(self.driver)
        self.loginPage.get_url()

    def test_login_success(self):
        self.loginPage.login(UserCredentials.valid_username, UserCredentials.valid_password)
        self.assertTrue("inventory.html" in self.driver.current_url)

    def test_login_fail(self):
        self.loginPage.login(UserCredentials.invalid_username, UserCredentials.invalid_password)
        self.assertTrue(self.loginPage.is_error_message_present())

    def test_login_locked(self):
        self.loginPage.login(UserCredentials.locked_username, UserCredentials.locked_password)
        self.assertTrue("locked out" in self.loginPage.get_error_message())


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(LoginTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
