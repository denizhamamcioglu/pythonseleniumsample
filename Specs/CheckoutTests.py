# coding=utf-8
import unittest
import UserCredentials
from selenium import webdriver
from PageObjects.LoginPage import LoginPage
from PageObjects.InventoryPage import InventoryPage
from PageObjects.CheckoutPage import CheckoutPage


class CheckoutTests(unittest.TestCase):
    def setUp(self):
        """
        Checkout related test cases.
        setUp block will be executed before each test case.
        """
        self.driver = webdriver.Chrome()
        self.loginPage = LoginPage.LoginPage(self.driver)
        self.inventoryPage = InventoryPage.InventoryPage(self.driver)
        self.checkoutPage = CheckoutPage.CheckoutPage(self.driver)
        self.loginPage.get_url()

    def test_checkout_success(self):
        self.loginPage.login(UserCredentials.valid_username, UserCredentials.valid_password)
        self.inventoryPage.click_add_to_cart_button(0)
        self.inventoryPage.click_shopping_cart()
        self.checkoutPage.click_checkout_button()
        self.checkoutPage.enter_firstname("First")
        self.checkoutPage.enter_lastname("Last")
        self.checkoutPage.enter_postal_code("060606")
        self.checkoutPage.click_continue_button()
        self.checkoutPage.click_continue_button()  # Click finish button, same class name
        self.assertTrue(self.checkoutPage.is_checkout_successful())

    def test_checkout_missing_mandatory_field_success(self):
        self.loginPage.login(UserCredentials.valid_username, UserCredentials.valid_password)
        self.inventoryPage.click_add_to_cart_button(0)
        self.inventoryPage.click_shopping_cart()
        self.checkoutPage.click_checkout_button()
        self.checkoutPage.enter_lastname("Last")
        self.checkoutPage.enter_postal_code("060606")
        self.checkoutPage.click_continue_button()
        self.assertTrue(self.checkoutPage.is_error_message_present())


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(CheckoutTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
