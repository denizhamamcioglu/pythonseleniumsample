# coding=utf-8
import unittest
from selenium import webdriver
from PageObjects.LoginPage import LoginPage
from PageObjects.InventoryPage import InventoryPage
import UserCredentials


class InventoryTests(unittest.TestCase):
    def setUp(self):
        """
        Shopping cart related test cases. Cases like add to cart, remove from cart are covered here.
        setUp block will be executed before each test case.
        """
        self.driver = webdriver.Chrome()
        self.loginPage = LoginPage.LoginPage(self.driver)
        self.inventoryPage = InventoryPage.InventoryPage(self.driver)
        self.loginPage.get_url()

    def test_add_item_to_cart_success(self):
        self.loginPage.login(UserCredentials.valid_username, UserCredentials.valid_password)
        self.inventoryPage.click_add_to_cart_button(0)
        self.assertTrue(self.inventoryPage.is_item_added_to_cart())

    def test_add_item_to_cart_fail(self):
        self.loginPage.login(UserCredentials.problematic_username, UserCredentials.problematic_password)
        self.inventoryPage.click_add_to_cart_button(2)
        self.assertTrue(self.inventoryPage.is_item_added_to_cart())

    def test_check_item_name_success(self):
        self.loginPage.login(UserCredentials.valid_username, UserCredentials.valid_password)
        self.inventoryPage.click_item_name(0)
        self.assertTrue("Backpack" in self.inventoryPage.get_item_name_details())

    def test_check_item_description_success(self):
        self.loginPage.login(UserCredentials.valid_username, UserCredentials.valid_password)
        self.inventoryPage.click_item_name(0)
        self.assertTrue("streamlined Sly Pack" in self.inventoryPage.get_item_description_details())

    def test_check_item_price_success(self):
        self.loginPage.login(UserCredentials.valid_username, UserCredentials.valid_password)
        self.inventoryPage.click_item_name(1)
        self.assertTrue("$9.99" in self.inventoryPage.get_item_price_details())

    def test_remove_item_from_cart_success(self):
        self.loginPage.login(UserCredentials.valid_username, UserCredentials.valid_password)
        self.inventoryPage.click_add_to_cart_button(0)
        self.inventoryPage.click_shopping_cart()
        self.inventoryPage.click_remove_from_cart_button(0)
        self.assertTrue(self.inventoryPage.is_item_removed_from_cart())

    def test_cross_check_item_name_fail(self):
        self.loginPage.login(UserCredentials.problematic_username, UserCredentials.problematic_password)
        item_name = self.inventoryPage.get_item_name(0)
        self.inventoryPage.click_item_name(0)
        item_name_details = self.inventoryPage.get_item_name_details()
        self.assertTrue(item_name in item_name_details)

    def test_cross_check_item_price_fail(self):
        self.loginPage.login(UserCredentials.problematic_username, UserCredentials.problematic_password)
        item_price = self.inventoryPage.get_item_price(0)
        self.inventoryPage.click_item_name(0)
        item_price_details = self.inventoryPage.get_item_price_details()
        self.assertTrue(item_price in item_price_details)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(InventoryTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
